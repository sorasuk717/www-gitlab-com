---
layout: markdown_page
title: "Database Sharding Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | February 11, 2020 |
| Target End Date | August 11, 2020|
| Slack           | [#wg_database-sharding](https://gitlab.slack.com/archives/CTNSZFHEZ) (only accessible from within the company) |
| Google Doc      | [Database Sharding Working Group Agenda](https://docs.google.com/document/d/1_sI-P2cLYPHlzDiJezI0YZHjWAC4BSKJ8aL0cNduDlo/edit#) (only accessible from within the company) |
| Issue Board     | TBD             |

## Business Goal

Database sharding and partitioning will improve availability, scalability and performance.  Sharding will also allow us to enable a path forward for data isolation.  As we continue to investigate implementations and technologies, we will test our hypotheses and add more detail about improvements in the areas below, listed in priority order.  

1. Availability - the database will no longer be a single point of failure as it is today.  Sharding will allow us to spread data across multiple servers and better isolate database outages
1. Scalability - sharding will allow us to horizontally scale at the database tier
1. Performance - partitioning will provide performance enhancements in several identified areas such as search and audit log tables

## Exit Criteria

- PostgreSQL 11 deployed on GitLab.com
  - Delivered as required in Omnibus (13.0)
- Deploy MVC partition
  - Define partition key (may be different than tenancy model for MVC)
  - Deploy
  - Measure results
- Implement sharding strategy
  - Identify shard key (e.g. Tenancy Model)
  - Implement and Demonstrate POCs
  - Gather feedback and metrics from POCs
  - Roll out sharding implementation

## Specific Lines of Enquiry

- [Upgrade to PostgreSQL 11 timeline](https://gitlab.com/groups/gitlab-org/-/epics/2184)
- [Infrastructure - Upgrade to PostgreSQL 11](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6505)
- Testing PostgreSQL 11 upgrade
- [Database Paritioning](https://gitlab.com/groups/gitlab-org/-/epics/2023)
- [Database Sharding](https://gitlab.com/groups/gitlab-org/-/epics/1854)

## Roles and Responsibilities

| Working Group Role                       | Person                          | Title                                    |
|------------------------------------------|---------------------------------|------------------------------------------|
| Executive Stakeholder | Christopher Lefelhocz           | Senior Director of Development           |
| Facilitator | Craig Gomes | Engineering Manager, Database            |
| DRI for Database Sharding | Craig Gomes | Engineering Manager, Database            |
| Functional Lead | TBD | Quality Engineering Manager, Dev         |
| Functional Lead | Josh Lambert | Senior Product Manager, Geo              |
| Functional Lead | Gerardo "Gerir" Lopez-Fernandez | Engineering Fellow, Infrastructure       |
| Functional Lead | Stan Hu                         | Engineering Fellow, Development          |
| Functional Lead | Andreas Brandl                  | Staff Backend Engineer, Database            |
| Member  | Chun Du | Director of Engineering, Enablement      |
| Member  | Pat Bair | Senior Backend Engineer, Database          |
