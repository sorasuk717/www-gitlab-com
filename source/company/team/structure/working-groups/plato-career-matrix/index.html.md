---
layout: markdown_page
title: "Plato Career Matrix Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    |  |
| Target End Date |  |
| Slack           | [#](https://gitlab.slack.com/) (only accessible from within the company) |
| Google Doc      | [Plato Career Matrix Working Group Agenda](https://docs.google.com/document/) (only accessible from within the company) |
| Issue Board     | [Issue board](https://gitlab.com/groups/gitlab-com/-/boards/)             |

## Business Goal

TODO

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Eric Johnson          | VP of Engineering              |
| Facilitator           | Roos Takken           | People Business Partner, Engineering |

## Exit Criteria

TODO