---
layout: markdown_page
title: "SDR Onboarding"
---

## The Goal of SDR Onboarding
As an SDR, you work closely with the larger marketing org as well as the sales team. Due to this, your onboarding will encompass training on tools and processes related to not only your role but a blend of marketing and sales. 

This learning experience involves virtual self-paced learning paths in the form of a general company-wide onboarding issue, an SDR specific issue, as well as a Google Classroom course to prep you for [Sales Quick Start (SQS)](/handbook/sales/onboarding/#sales-quick-start-workshop). SQS is a 3-day in-person immersive and hands-on workshop for all new hires in an SDR or sales role. In parallel, you will meet regularly with your manager, onboarding buddy and members of our SDR enablement team to chat through and dig a bit deeper into the topics within your onboarding. 

The goal in all of this is to ensure that you feel confident and comfortable to hit the ground running at the start of your second month on the job when you will have a [ramped quota](/handbook/marketing/revenue-marketing/sdr/#sdr-compensation-and-quota). Our hope is that along this journey you are not only gaining the tactical skills needed to complete your job, but that you form connections with colleagues, gain a strong understanding of our culture, and begin to develop industry knowledge.

## SDR Onboarding Process

1.  The People Team initiates a general GitLab onboarding issue for every new GitLab team member. On your first day, you will receive a welcome email with a link to your specific onboarding issue and steps to get started. You will also meet with your manager to discuss:
    * Access to your SDR specific issue
    * Prioritizing your onboarding issues
    * How to manage calendar invites in your inbox on Day 1
1.  Within 3 days of starting at GitLab, you will receive an email giving you access to [Command of the Message (CoM)](/handbook/sales/command-of-the-message/) e-learning materials. CoM is our value-driven conversation framework that will be covered during SQS.
1.  During your second week, the Sales Enablement Team will send you a calendar invite for the upcoming SQS as well as an email prompting you to log in to Google Classroom to begin working through the [Sales Quick Start learning path](/handbook/sales/onboarding/#sales-quick-start-learning-path). Connect with your manager to arrange travel for SQS or if you are unable to attend the next session. 
2.  Within a week of attending SQS, you will receive access to the 13-week Command of the Message Fast Start program.

## Graduating from SDR Onboarding
* Complete GitLab general onboarding issue
* Complete SDR specific onboarding issue
* Complete Google Classroom SQS prep work
* Attend the SQS workshop
* Complete the 13-week Command of the Message Fast Start program

## Manager Responsibilities

1.  Complete all manager specific tasks in both the general onboarding issue as well as the SDR specific issue.
    *  Quick Reminder - prior to day 1, you will want to schedule meetings with new hires to discuss the following topics:
        * Day 1: 
             *  Welcome to GitLab, walkthrough of their issues, calendar event invites in their inbox, slack channels, spending company money. 
        * Throughout their first two weeks:
            * SDR compensation and quota
            * SAO criteria 
            * Working with a SAL/AE
            * Submit an [access request](https://gitlab.com/gitlab-com/access-requests/issues) for dev access. Provide the necessary information at the top but you will only need to request dev access. All of the other tools/groups will be provisioned in advance or throughout their onboarding. 

